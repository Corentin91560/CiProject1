package com.example.benevent;

import android.view.View;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.benevent.Activity.SigninActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UITest {

    @Rule
    public ActivityScenarioRule<SigninActivity> activityScenarioRule
            = new ActivityScenarioRule<>(SigninActivity.class);

    @Test
    public void goToSignUp() {
        onView(withId(R.id.no_account_button))
                .perform(click());
        onView(withId(R.id.ed_name_signup)).check(matches(isDisplayed()));
    }

    @Test
    public void PerformSigninValide() {
        onView(withId(R.id.log_ed)).perform(typeText("jd@example.fr"));
        onView(withId(R.id.pass_ed)).perform(typeText("DoeJohn"));
        onView(ViewMatchers.isRoot()).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button_signin))
                .perform(click());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Déconnexion"))
                .perform(click());
        onView(withText("DÉCONNEXION"))
                .perform(click());
        onView(withId(R.id.log_ed)).check(matches(isDisplayed()));
    }

    @Test
    public void PerformSigninFail() {
        onView(withId(R.id.log_ed)).perform(typeText("test"));
        onView(withId(R.id.pass_ed)).perform(typeText("test"));
        onView(ViewMatchers.isRoot()).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button_signin))
                .perform(click());
        onView(withId(R.id.button_signin)).check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void goToAssociationFragment(){
        onView(withId(R.id.log_ed)).perform(typeText("jd@example.fr"));
        onView(withId(R.id.pass_ed)).perform(typeText("DoeJohn"));
        onView(ViewMatchers.isRoot()).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button_signin))
                .perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.drawer_layout))
                .perform(DrawerActions.open());
        onView(withId(R.id.nav_view))
                .perform(NavigationViewActions.navigateTo(R.id.nav_asso));

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Déconnexion"))
                .perform(click());
        onView(withText("DÉCONNEXION"))
                .perform(click());
        onView(withId(R.id.log_ed)).check(matches(isDisplayed()));
    }

    @Test
    public void navigateIntoEventDetail(){
        onView(withId(R.id.log_ed)).perform(typeText("jd@example.fr"));
        onView(withId(R.id.pass_ed)).perform(typeText("DoeJohn"));
        onView(ViewMatchers.isRoot()).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button_signin))
                .perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.drawer_layout))
                .perform(DrawerActions.open());
        onView(withId(R.id.nav_view))
                .perform(NavigationViewActions.navigateTo(R.id.nav_event));

        onView(withId(R.id.recycler_event)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, MyViewAction.clickChildViewWithId(R.id.button_event_view)));

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Déconnexion"))
                .perform(click());
        onView(withText("DÉCONNEXION"))
                .perform(click());
        onView(withId(R.id.log_ed)).check(matches(isDisplayed()));
    }

    @Test
    public void checkBackButtonWhileLog(){
        onView(withId(R.id.log_ed)).perform(typeText("jd@example.fr"));
        onView(withId(R.id.pass_ed)).perform(typeText("DoeJohn"));
        onView(ViewMatchers.isRoot()).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.button_signin))
                .perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(isRoot()).perform(ViewActions.pressBack());
        onView(withText("OUI"))
                .perform(click());
    }
}

class MyViewAction {

    public static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on a child view with specified id.";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(id);
                v.performClick();
            }
        };
    }

}