package com.example.benevent;

import com.example.benevent.Activity.SigninActivity;
import com.example.benevent.Activity.SignupActivity;
import com.example.benevent.Models.Association;
import com.example.benevent.Models.Event;
import com.example.benevent.Models.Feedback;
import com.example.benevent.Models.Follow;
import com.example.benevent.Models.Login;
import com.example.benevent.Models.Signup;
import com.example.benevent.Models.User;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void userCreation(){
        User testedUser = new User("jean","denis","0667440087","lienverslaprofilpicture");

        assertEquals("jean", testedUser.getName());
        assertEquals("denis", testedUser.getFirstname());
        assertEquals("0667440087", testedUser.getPhone());
        assertEquals("lienverslaprofilpicture", testedUser.getProfilpicture());

    }

    @Test
    public void assoCreation(){
        Association testedAsso = new Association(1, "Test",  "urldelasso", "TST", "test@test.fr", "0610101010", "http://test.fr", "http://test.fr/support", "password", 1);

        assertEquals("Test", testedAsso.getName());
        assertEquals("urldelasso", testedAsso.getLogo());
        assertEquals("TST", testedAsso.getAcronym());
        assertEquals("test@test.fr", testedAsso.getEmail());
        assertEquals("http://test.fr", testedAsso.getWebsite());
        assertEquals("http://test.fr/support", testedAsso.getSupport());
        assertEquals(1, testedAsso.getIdcat());
        assertEquals(1, testedAsso.getIdas());

    }

    @Test
    public void followCreation(){
        Follow testedFollow = new Follow(1,1);

        assertEquals(1, testedFollow.getIdu());
        assertEquals(1, testedFollow.getIdas());

    }

    @Test
    public void feedbackWithNoteCreation(){
        Feedback testedFeedbackWithNote = new Feedback(1, "feedback de test","2014-17-21",5,1,"ANDROID");

        assertEquals("feedback de test", testedFeedbackWithNote.getContent());
        assertEquals("2014-17-21", testedFeedbackWithNote.getDate());

    }

    @Test
    public void feedbackWithoutNoteCreation(){
        Feedback testedFeedbackWithNote = new Feedback(1, "feedback de test","feedback de qualité","2014-17-21",1,"ANDROID");

        assertEquals("feedback de qualité", testedFeedbackWithNote.getContent());
        assertEquals("2014-17-21", testedFeedbackWithNote.getDate());

    }
}
